//
//  ECRefreshControlTableViewControler.m
//  ECRefreshControlTableViewControlerDemo
//
//  Created by PO-YU SU on 2014/8/7.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import "ECRefreshControlTableViewControler.h"

@interface ECRefreshControlTableViewControler ()
@property (nonatomic) BOOL isMoreRefresh;
@property (nonatomic) BOOL refreshing;
@end

@implementation ECRefreshControlTableViewControler

- (void)refreshViewControlEventValueChanged
{
    self.refreshing = YES;
    
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"更新中..."];
    
    [self.refreshDelegate startRefreshFromRefreshControlTableView:self.tableView];
}

- (void)updateMoreRefresh
{
    UITableViewCell *cell = [self.tableView.visibleCells lastObject];
    if (![cell.reuseIdentifier isEqualToString:ECRefreshControlTableViewCellIdentifier])
        return;
    
    UIButton *button = (UIButton *)[cell viewWithTag:1];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[cell viewWithTag:2];
    
    button.hidden = self.isMoreRefresh;
    indicatorView.hidden = !self.isMoreRefresh;
    (self.isMoreRefresh)?[indicatorView startAnimating]:[indicatorView stopAnimating];
}

- (void)addRefreshControl
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"下拉更新"];
    [refreshControl addTarget:self
                       action:@selector(refreshViewControlEventValueChanged)
             forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refreshControl;
}

- (void)stopRefresh
{
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
    
    self.refreshing = NO;
}

- (void)stopMoreRefresh
{
    NSInteger section = [self.refreshDataSource numberOfSectionsInRefreshCTableView:self.tableView] - 1;
    NSInteger tableRows = [self.tableView numberOfRowsInSection:section] - 1;
    NSInteger arrayRows = [self.refreshDataSource refreshControlTableView:self.tableView numberOfRowsInSection:section];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSInteger row = tableRows; row < arrayRows; row++)
    {
        [indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
    }
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    self.isMoreRefresh = NO;
    [self updateMoreRefresh];
    
    self.refreshing = NO;
    [self addRefreshControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.moreMode = YES;
    
    [self addRefreshControl];
    
//    self.tableView.contentOffset = CGPointMake(0, -self.refreshControl.frame.size.height);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.refreshControl beginRefreshing];
    
    [self.refreshDelegate startRefreshFromRefreshControlTableView:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)startRefresh
{
    if (!self.refreshControl.refreshing)
    {
        self.refreshing = NO;
        [self.refreshControl beginRefreshing];
        [self.refreshDelegate startRefreshFromRefreshControlTableView:self.tableView];
    }
}

- (void)stop
{
    if (self.refreshControl.refreshing)
    {
        [self stopRefresh];
    }
    else if (self.isMoreRefresh)
    {
        [self stopMoreRefresh];
    }
}
#pragma mark IBAction
- (IBAction)touchMoreButton:(id)sender
{
    if (self.refreshing)
        return;
    
    self.refreshing = YES;
    self.refreshControl = nil;
    
    self.isMoreRefresh = YES;
    [self updateMoreRefresh];
    
    if ([self.refreshDelegate respondsToSelector:@selector(startMoreRefreshFromRefreshControlTableView:)])
    {
        [self.refreshDelegate startMoreRefreshFromRefreshControlTableView:self.tableView];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger number = [self.refreshDataSource numberOfSectionsInRefreshCTableView:tableView];

    return number;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = [self.refreshDataSource refreshControlTableView:tableView numberOfRowsInSection:section];
    
    return number += (self.moreMode)?1:0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger lastRow = [self.refreshDataSource refreshControlTableView:tableView numberOfRowsInSection:indexPath.section];
    UITableViewCell *cell;
    if (self.moreMode)
    {
        if (indexPath.row == lastRow || lastRow == 0)
        {
            cell = [self.tableView dequeueReusableCellWithIdentifier:ECRefreshControlTableViewCellIdentifier];
            
            UIButton *button = (UIButton *)[cell viewWithTag:1];
            if (!button)
            {
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setTag:1];
                [button setFrame:cell.frame];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button setTitle:@"取得更多" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(touchMoreButton:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:button];
            }
            
            UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[cell viewWithTag:2];
            if (!indicatorView)
            {
                indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [indicatorView setTag:2];
                indicatorView.center = cell.center;
                indicatorView.color = [UIColor darkGrayColor];
                [cell addSubview:indicatorView];
            }
            
            button.hidden = self.isMoreRefresh;
            indicatorView.hidden = !self.isMoreRefresh;
            (self.isMoreRefresh)?[indicatorView startAnimating]:[indicatorView stopAnimating];
            return cell;
        }
    }
    
    cell = [self.refreshDataSource refreshControlTableView:tableView cellForRowAtIndexPath:indexPath];

    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger lastRow = [self.refreshDataSource refreshControlTableView:tableView numberOfRowsInSection:indexPath.section];
    if (indexPath.row == lastRow)
        return;
    
    if (self.refreshDelegate)
    {
        if ([self.refreshDelegate respondsToSelector:@selector(refreshControlTableView:didSelectRowAtIndexPath:)])
        {
            [self.refreshDelegate refreshControlTableView:tableView didSelectRowAtIndexPath:indexPath];
        }
    }
}

@end
