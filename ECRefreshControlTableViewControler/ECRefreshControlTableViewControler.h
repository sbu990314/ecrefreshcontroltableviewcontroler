//
//  ECRefreshControlTableViewControler.h
//  ECRefreshControlTableViewControlerDemo
//
//  Created by PO-YU SU on 2014/8/7.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ECRefreshControlTableViewCellIdentifier @"ECRefreshCell"

@protocol ECRefreshControlTableViewDataSource;
@protocol ECRefreshControlTableViewDelegate;

@interface ECRefreshControlTableViewControler : UITableViewController
@property (nonatomic, assign)   id <ECRefreshControlTableViewDataSource> refreshDataSource;
@property (nonatomic, assign)   id <ECRefreshControlTableViewDelegate>   refreshDelegate;

@property (nonatomic) BOOL moreMode;

- (void)startRefresh;
- (void)stop;
@end

@protocol ECRefreshControlTableViewDataSource <NSObject>
- (NSInteger)refreshControlTableView:(UITableView *)tableView
               numberOfRowsInSection:(NSInteger)section;

- (NSInteger)numberOfSectionsInRefreshCTableView:(UITableView *)tableView;

- (UITableViewCell *)refreshControlTableView:(UITableView *)tableView
                       cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end


@protocol ECRefreshControlTableViewDelegate <NSObject>
@required
- (void)refreshControlTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)startRefreshFromRefreshControlTableView:(UITableView *)tableView;

@optional
- (void)startMoreRefreshFromRefreshControlTableView:(UITableView *)tableView;
@end