//
//  ViewController.m
//  ECRefreshControlTableViewControlerDemo
//
//  Created by PO-YU SU on 2014/8/7.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import "ViewController.h"
#import "ECRefreshControlTableViewControler.h"

@interface ViewController () <ECRefreshControlTableViewDataSource, ECRefreshControlTableViewDelegate>
@property (weak, nonatomic) ECRefreshControlTableViewControler *tableViewController;
@property (strong ,nonatomic) NSMutableArray *array;
@end

@implementation ViewController

- (void)resetArray
{
    self.array = [NSMutableArray array];
    for (NSInteger index = 0; index < 8; index++)
    {
        [self.array addObject:[NSNumber numberWithInteger:(arc4random() % 100)]];
    }
    
    [self.tableViewController stopRefresh];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.array = [NSMutableArray array];
    
    ECRefreshControlTableViewControler *tableViewController = (ECRefreshControlTableViewControler *)[self.storyboard instantiateViewControllerWithIdentifier:@"ECRefreshControlTableViewControler"];
    tableViewController.refreshDataSource = self;
    tableViewController.refreshDelegate = self;
    [self.view addSubview:tableViewController.tableView];
    CGRect frame = tableViewController.tableView.frame;
    frame.size.height = 300;
    tableViewController.tableView.frame = frame;
    NSLog(@"%@", tableViewController.tableView);
    [self addChildViewController:tableViewController];
    
    self.tableViewController = tableViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)refreshControlTableView:(UITableView *)tableView
               numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInRefreshCTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)refreshControlTableView:(UITableView *)tableView
                       cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [[self.array objectAtIndex:indexPath.row] stringValue];
    
    return cell;
}

- (UITableViewCell *)refreshMoreCellFromTableView:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ECRefreshCell"];
    
    return cell;
}

- (void)refreshControlTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)startRefreshFromRefreshControlTableView:(UITableView *)tableView
{
    [self performSelector:@selector(resetArray) withObject:nil afterDelay:1.0f];
}

- (void)startMoreRefreshFromRefreshControlTableView:(UITableView *)tableView
{
    NSInteger num = [[self.array lastObject] intValue];
    NSNumber *object = [NSNumber numberWithInteger:++num];
    [self.array addObject:object];
    object = [NSNumber numberWithInteger:++num];
    [self.array addObject:object];
    object = [NSNumber numberWithInteger:++num];
    [self.array addObject:object];
    object = [NSNumber numberWithInteger:++num];
    [self.array addObject:object];
    [self.tableViewController performSelector:@selector(stopMoreRefresh) withObject:nil afterDelay:1.0f];
}

@end
