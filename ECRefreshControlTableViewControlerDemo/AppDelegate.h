//
//  AppDelegate.h
//  ECRefreshControlTableViewControlerDemo
//
//  Created by PO-YU SU on 2014/8/7.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
